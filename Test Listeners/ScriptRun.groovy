import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.xml.bind.JAXBElement.GlobalScope

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import reports.extentReports
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;

class ScriptRun {
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	//private static ExtentReports extent;
	//ExtentTest test
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
		def testcaseName=testCaseContext.testCaseId.split("/")
		GlobalVariable.testCase=testcaseName[1]
		println GlobalVariable.testCase
		if(!GlobalVariable.testCase.equals("Login")){
			ExtentReports report = GlobalVariable.extentrep
			ExtentTest test = report.startTest(GlobalVariable.ENVData+"_"+GlobalVariable.testCase)
			GlobalVariable.testrep = test
			GlobalVariable.testRun=CustomKeywords.'runSelection.testcaseExecution.testcaseRun'(GlobalVariable.testCase,GlobalVariable.testSuite)
			println GlobalVariable.testRun
			if(!GlobalVariable.testRun.contains('Y')){
				test.log(LogStatus.SKIP, GlobalVariable.testCase)
			}
			if(GlobalVariable.runStatus == false){
				WebUI.closeBrowser()
				GlobalVariable.runStatus = true
				WebUI.callTestCase(findTestCase('Test Cases/Login'), null, FailureHandling.STOP_ON_FAILURE)
				
			}
		}
	
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
		ExtentTest test = GlobalVariable.testrep
		if(!GlobalVariable.testCase.equals("Login")){
			if(GlobalVariable.testRun.contains('Y') && GlobalVariable.runStatus==true){
				
				test.log(LogStatus.PASS,GlobalVariable.ENVData+"_"+GlobalVariable.testCase);
			}
			else{
				test.log(LogStatus.FAIL,GlobalVariable.ENVData+"_"+GlobalVariable.testCase)
			}
			
		}
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
		//ExtentReports rep = CustomKeywords.'reports.extentReports.getInstance'()
		//GlobalVariable.extentrep =rep
		
		def testSuiteName =testSuiteContext.testSuiteId.split("/")
		GlobalVariable.testSuite=testSuiteName[2]
		println GlobalVariable.testSuite

	}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
		ExtentReports report = GlobalVariable.extentrep
		ExtentTest test = GlobalVariable.testrep
		report.endTest(test);
		report.flush();
	}
}