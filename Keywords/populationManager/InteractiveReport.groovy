								package populationManager

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.InputEvent
import java.awt.event.KeyEvent
import java.lang.annotation.ElementType
import java.lang.reflect.WeakCache.Value

import javax.swing.plaf.basic.BasicFileChooserUI.DoubleClickListener

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By.ByLinkText
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import org.apache.ivy.plugins.lock.FileBasedLockStrategy.CreateFileLocker
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.apache.commons.lang.StringUtils;

public class InteractiveReport {
	@Keyword
	def ScrollUsingJs() {
		WebDriver driver = DriverFactory.getWebDriver()
		JavascriptExecutor js = ((driver) as JavascriptExecutor)
		js.executeScript("window.scrollBy(0, 250)", "");

		/*JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("javascript:window.scrollBy(250,350)");
		 WebDriver driver = DriverFactory.getWebDriver()
		 JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		 executor.executeScript("javascript:window.scrollBy(250,350)");*/
	}
	@Keyword
	def DoubleClickUsingJS(TestObject to, int timeout) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(to, timeout)
		JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		executor.executeScript("arguments[0].dispatchEvent(new CustomEvent('contextmenu'))", element);
		executor.executeScript("arguments[0].dispatchEvent(new CustomEvent('dblclick'))", element);
	}
	@Keyword
	def dispatchEventClickUsingJS(TestObject to, int timeout) {

		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(to, timeout)
		JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		executor.executeScript("arguments[0].dispatchEvent(new CustomEvent('contextmenu'))", element);
		executor.executeScript("arguments[0].dispatchEvent(new CustomEvent('click'))", element);
	}
	@Keyword
	def clickUsingJS(TestObject to, int timeout) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(to, timeout)
		JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		executor.executeScript('arguments[0].click()', element)
	}

	@Keyword
	def keypressUsingJS() {
		Robot robot = new java.awt.Robot()
		//Robot KeyEvent =java.awt.event.KeyEvent
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WebUI.delay(5)
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WebUI.delay(5)
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WebUI.delay(5)
		//			robot.keyPress(KeyEvent.VK_DOWN);
		//		    robot.keyRelease(KeyEvent.VK_ENTER);
		//		    WebUI.delay(5)
		//			robot.keyPress(KeyEvent.VK_DOWN);
		//			robot.keyRelease(KeyEvent.VK_ENTER);
		//	         WebUI.delay(5)
		//		    robot.keyPress(KeyEvent.VK_ENTER);
		//		    robot.keyRelease(KeyEvent.VK_ENTER);
		//			WebUI.delay(5)
		//			robot.keyPress(KeyEvent.VK_ENTER);
		//			robot.keyRelease(KeyEvent.VK_ENTER);
		//			WebUI.delay(5)
		//			robot.keyPress(KeyEvent.VK_ENTER);
		//			robot.keyRelease(KeyEvent.VK_ENTER);
		//			WebUI.delay(5)

		//			robot.keyPress(KeyEvent.VK_ENTER);
		//			robot.keyRelease(KeyEvent.VK_ENTER);
		//			WebUI.delay(5)



	}
	@Keyword
	def CreateFile() {
		File file = new File("C:\\Users\\qatester4\\Downloads\\Backupfrom");
		file.createNewFile();
		/*File from = new File("C:\\Users\\qatester4\\Downloads\\Assessments_Excel");
		 File to = new File("C:\\Users\\qatester4\\Downloads\\Backupexcel");
		 from.renameTo(from)*/
	}
	@Keyword
	def PressUsingJs(){
		Robot r = new java.awt.Robot()
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_ENTER);
		//r.keyPress(KeyEvent.VK_ENTER);
		//r.keyRelease(KeyEvent.VK_ENTER);
	}

	@Keyword
	def dragAndDrop(TestObject sourceObject, TestObject destinationObject) {
		WebElement sourceElement = WebUiBuiltInKeywords.findWebElement(sourceObject);
		WebElement destinationElement = WebUiBuiltInKeywords.findWebElement(destinationObject);
		WebDriver webDriver = DriverFactory.getWebDriver();
		((JavascriptExecutor) webDriver).executeScript(getJsDndHelper() + "simulateDragDrop(arguments[0], arguments[1])", sourceElement, destinationElement)
	}

	@Keyword
	def boolean isNumericValue(String str){
		return StringUtils.isNumeric(str);
	}
}



