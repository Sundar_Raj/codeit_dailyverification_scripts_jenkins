package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.junit.After
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.FindBy
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.GraphicsConfiguration.DefaultBufferCapabilities
import java.awt.TexturePaintContext.Int
import java.lang.String.CaseInsensitiveComparator
import java.lang.invoke.SwitchPoint
import java.time.format.DateTimeFormatterBuilder.InstantPrinterParser
import java.util.Iterator

public class CareTeamWizard extends DemographicsApp {

	@Keyword
	public navigateCareTeamWizard(String patientID, String patientLastName) {
		DemographcisAppNavigation()
		WebUI.click(findTestObject('Application/CareTeamApp/SearchPatientPages/CTA_SPP_ResetAppButton'))
		searchPatientInDemographcisApp(patientID, patientLastName)
		WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_selectButton'), FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_EnrollmentButton'), FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamWizard'), FailureHandling.STOP_ON_FAILURE)
	}

	@Keyword
	public removeMember() {
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'))
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'))
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_UnassigningCareTeamPopupYesButton'))
	}

	@Keyword
	public addMember(String CareTeamAdd){
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_AddMemberButton'))
		WebUI.sendKeys(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/AddNewMemberWindow/EACW_CTW_ADNMW_AddMemberSearch'), CareTeamAdd)
		TestObject careTeamMemberSelect = findTestObject('Base/commanXpath')
		careTeamMemberSelect.findProperty('xpath').setValue("//md-list-item/div/button[contains(@aria-label,'"+CareTeamAdd+"')]")
		WebUI.click(careTeamMemberSelect)
	}

	@Keyword
	public navigateAddNewMemberPage(){
		try{
			WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_AddMemberButton'),FailureHandling.STOP_ON_FAILURE)
			boolean navigatePageStatus=WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/AddNewMemberWindow/EACW_CTW_ADNMW_AddMemberSearch'), 5, FailureHandling.STOP_ON_FAILURE)
			return navigatePageStatus
		}catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean careteamassign(String ExistingCareTeamNameSelect){
		WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamWizard'), 5, FailureHandling.OPTIONAL)
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamWizard'))
		WebUI.delay(3)
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CareTeamMemberCard'), 5, FailureHandling.OPTIONAL)){
			WebUI.delay(3)
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_DotVerticalButton'))
			WebUI.delay(3)
			WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_RemovePatientButton'))
			WebUI.delay(3)
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_UnassigningCareTeamPopupYesButton'))
			WebUI.delay(3)
		}
		WebUI.sendKeys(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_CTW_CareTeamWizardSearchTab'),ExistingCareTeamNameSelect)
		TestObject existingCareTeamNameSelect = findTestObject('Base/commanXpath')
		existingCareTeamNameSelect.findProperty('xpath').setValue("//span[contains(@md-highlight-text,'teamSearch')]/span[text()='"+ExistingCareTeamNameSelect+"']")
		WebUI.delay(3)
		WebUI.click(existingCareTeamNameSelect,FailureHandling.STOP_ON_FAILURE)

		//click save button
		WebUI.click(findTestObject('Object Repository/Application/EnrollmentAndCareteamWizard/CareTeamWizard/EACW_SaveButton'))
		WebUI.delay(5)
		return true
	}
}
