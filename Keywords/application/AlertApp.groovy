package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class AlertApp {

	@Keyword
	public Boolean alertappnavigate(){
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"),FailureHandling.STOP_ON_FAILURE)
		Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"), 5,FailureHandling.STOP_ON_FAILURE)
		return appNavigationStatus
	}
	@Keyword
	public Boolean dropdownone(){
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppDropDownSelect"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertAppDropDownSelect"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SelectApplication"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SelectApplication"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		return true
	}
	@Keyword
	public Boolean dropdowntwo(){
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertComboBox"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertComboBox"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SelectCarePlan"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SelectCarePlan"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SearchClick"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SearchClick"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		return true
	}
}
