 package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper


import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class PatientPanel {
	@Keyword
	public String searchPatientInThePatientPanel(String patientId,String patientLastName){
		String Status= "";
		WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_SearchField"), 5, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(2)
		WebUI.clearText(findTestObject('Applications/PatientPanel/PP_SearchField'))
		//WebUiCommonHelper.findWebElement(findTestObject('Applications/PatientPanel/PP_SearchField'), 8).clear()
		WebUI.delay(2)
		WebUI.setText(findTestObject("Applications/PatientPanel/PP_SearchField"), patientId, FailureHandling.STOP_ON_FAILURE)
		TestObject patientSearchObject=findTestObject("Base/commanXpath")
		//patientSearchObject.findProperty("xpath").setValue("//div[1][contains(text(),'"+patientLastName+"')]//following::div[1]/div[contains(text(),'"+patientId+"')]")
		patientSearchObject.findProperty("xpath").setValue("//h4[@class='info']/span[text()= 'ID: "+patientId+"'] | //div[@class='text-faded demog']//div[text()='ID: "+patientId+"']")
		WebUI.delay(4)
		if(WebUI.verifyElementPresent(patientSearchObject, 5, FailureHandling.OPTIONAL)){
			Status="PatientListedInThePatientPanel"
		}else if(WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_NoPatientsFoundText"), 5, FailureHandling.OPTIONAL)){
			Status="NoPatientRecordFound"
		}else if(WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_ErrorOccuredMessage"), 1, FailureHandling.OPTIONAL)){
			Status="When user search a patient in the patient panel. The 'Error occurred' error message is displayed"
		}
		return Status
	}

	public String removePatientInThePatientPanel(String patientId, String patientLastName){
		String searchPatient=searchPatientInThePatientPanel(patientId, patientLastName)
		if(!searchPatient.contentEquals("PatientListedInThePatientPanel")){
			return searchPatient
		}
		WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_VerticalEllipsisIcon"), 5, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Applications/PatientPanel/PP_VerticalEllipsisIcon"), FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_RemovePatientFromPanelOption"), 5, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Applications/PatientPanel/PP_RemovePatientFromPanelOption"), FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementPresent(findTestObject("Applications/PatientPanel/PP_RemovePatientFromPanelPopup"), 5, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Applications/PatientPanel/PP_RemovePatientFromPanelPopup"), FailureHandling.STOP_ON_FAILURE)
		String searchRemovedPatient=searchPatientInThePatientPanel(patientId,patientLastName)
		WebUI.verifyMatch(searchRemovedPatient, "NoPatientRecordFound", false)
		return searchRemovedPatient
	}
}

