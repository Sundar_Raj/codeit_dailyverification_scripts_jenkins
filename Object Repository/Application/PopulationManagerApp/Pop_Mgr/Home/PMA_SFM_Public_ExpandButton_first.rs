<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PMA_SFM_Public_ExpandButton_first</name>
   <tag></tag>
   <elementGuidId>a6f99534-87c8-450d-a4df-16b5edc3195d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;fb674f9c-0d5f-4f07-8d6d-3ad92a7ec416&quot;)[@class=&quot;folder first open&quot;][count(. | //div[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/Browser_iframe']) = count(//div[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/Browser_iframe'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fb674f9c-0d5f-4f07-8d6d-3ad92a7ec416&quot;)[@class=&quot;folder first open&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>expandCollapse</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/Browser_iframe</value>
   </webElementProperties>
</WebElementEntity>
