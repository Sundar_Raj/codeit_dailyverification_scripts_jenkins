<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Browse Files</name>
   <tag></tag>
   <elementGuidId>35b8ad27-cacd-403f-8f1e-1eb1aad70d12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;buttonWrapper&quot;)/div[@class=&quot;well sidebar&quot;]/button[@class=&quot;btn btn-large btn-block&quot;][count(. | //button[@onclick = concat('window.top.mantle_setPerspective(' , &quot;'&quot; , 'browser.perspective' , &quot;'&quot; , ')') and @ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_home.perspective']) = count(//button[@onclick = concat('window.top.mantle_setPerspective(' , &quot;'&quot; , 'browser.perspective' , &quot;'&quot; , ')') and @ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_home.perspective'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonWrapper&quot;)/div[@class=&quot;well sidebar&quot;]/button[@class=&quot;btn btn-large btn-block&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.top.mantle_setPerspective('browser.perspective')</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Browse Files</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-large btn-block</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/iframe_home.perspective</value>
   </webElementProperties>
</WebElementEntity>
