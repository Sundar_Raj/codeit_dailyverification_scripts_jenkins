<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ProviderID</name>
   <tag></tag>
   <elementGuidId>658a3130-ae35-4aa4-8342-d7dbf424f9df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Provider ID']/following::tr/td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Provider ID']/following::tr/td[1]</value>
   </webElementProperties>
</WebElementEntity>
