<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CareTeam-ALL</name>
   <tag></tag>
   <elementGuidId>dafcadb7-dd3c-45a4-9f81-e3466462cced</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Care Team Role']/following::select[1]/option[@value='ALL']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Care Team Role']/following::select[1]/option[@value='ALL']</value>
   </webElementProperties>
</WebElementEntity>
