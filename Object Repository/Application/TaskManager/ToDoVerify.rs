<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ToDoVerify</name>
   <tag></tag>
   <elementGuidId>2b98e4a8-6096-49de-a437-353171d0910b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='To Do']/../following::*/div[contains(@class,'task-item-text')]/h3[text()]</value>
   </webElementProperties>
</WebElementEntity>
