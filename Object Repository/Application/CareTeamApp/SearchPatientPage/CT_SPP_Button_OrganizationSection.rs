<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_SPP_Button_OrganizationSection</name>
   <tag></tag>
   <elementGuidId>337eb7a7-c398-4a4d-b67b-1f3b31373448</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[text()=&quot;Organization&quot;])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[text()=&quot;Organization&quot;])[1]</value>
   </webElementProperties>
</WebElementEntity>
