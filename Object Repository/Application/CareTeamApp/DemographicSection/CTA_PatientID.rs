<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_PatientID</name>
   <tag></tag>
   <elementGuidId>052ca29b-7b99-41e1-bd9d-26fab5e0bae4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='tabappCareteams']//label[text()=&quot;Patient ID&quot;]/..//following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='tabappCareteams']//label[text()=&quot;Patient ID&quot;]/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
