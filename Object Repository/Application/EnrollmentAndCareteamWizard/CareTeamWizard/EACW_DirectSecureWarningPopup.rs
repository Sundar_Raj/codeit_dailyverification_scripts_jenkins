<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_DirectSecureWarningPopup</name>
   <tag></tag>
   <elementGuidId>64799462-808a-4fac-b909-d3d3bc5ef6af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label=&quot;Warning!Direct messaging requires ...&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@aria-label=&quot;Warning!Direct messaging requires ...&quot;]</value>
   </webElementProperties>
</WebElementEntity>
