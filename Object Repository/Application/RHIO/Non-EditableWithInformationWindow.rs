<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Non-EditableWithInformationWindow</name>
   <tag></tag>
   <elementGuidId>3d1d1039-7c34-4bf1-984c-78f9d1b8ed7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'The system automatically shares and receives information')]</value>
   </webElementProperties>
</WebElementEntity>
