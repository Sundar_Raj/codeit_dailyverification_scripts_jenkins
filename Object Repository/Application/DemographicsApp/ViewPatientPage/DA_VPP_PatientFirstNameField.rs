<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_PatientFirstNameField</name>
   <tag></tag>
   <elementGuidId>439d584e-e903-4d64-a3e1-4fb66fa7794c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='First Name']/following::td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='First Name']/following::td[1]</value>
   </webElementProperties>
</WebElementEntity>
