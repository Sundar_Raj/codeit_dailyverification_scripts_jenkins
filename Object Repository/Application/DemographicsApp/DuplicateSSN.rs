<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DuplicateSSN</name>
   <tag></tag>
   <elementGuidId>7c977a9e-3492-4d3f-aae4-e2983797f7f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()=Error updating patient. This patient has the same SSN as one other patient. That patient is assigned to A MMC.']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()=Error updating patient. This patient has the same SSN as one other patient. That patient is assigned to A MMC.']</value>
   </webElementProperties>
</WebElementEntity>
